#! /usr/bin/env python


#  Python program -- demo for Raspberry Pi

import sys;

# Data taken from https://github.com/mcewand/Morse-Code-Translator

morseDict = {
		'a':'13',
		'b':'3111',
		'c':'3131',
		'd':'311',
		'e':'1',
		'f':'1131',
		'g':'331',
		'h':'1111',
		'i':'11',
		'j':'1333',
		'k':'313',
		'l':'1311',
		'm':'33',
		'n':'31',
		'o':'333',
		'p':'1331',
		'q':'3313',
		'r':'131',
		's':'111',
		't':'3',
		'u':'113',
		'v':'1113',
		'w':'133',
		'x':'3113',
		'y':'3133',
		'z':'3311',
		'1':'13333',
		'2':'11333',
		'3':'11133',
		'4':'11113',
		'5':'11111',
		'6':'31111',
		'7':'33111',
		'8':'33311',
		'9':'33331',
		'0':'33333',
    };

ILS = -3;  # Interletter space

WHITESPACE = -7; #Between words

def encodeMsg(tMsg):
    # Convert a text message to a list of morse strings
    eMsg = list(); # Encoded message
    inWord = False;

    for char in tMsg:
        if not char == ' ':
            if inWord:
                eMsg.append(ILS);

            eMsg.append(morseDict[char]);
            inWord = True;
        else:
            inWord = False;
            eMsg.append(WHITESPACE);
        #endif
    #endfor

    return eMsg;

#enddef


def decode(mMessage):
    return 0;
#enddef

def parse(tMessage):
    # Convert punctuation to whitespace, convert runs of whitespace to single space
    # make all chars lower case
    # Strip trailing white space

    pMsg = list(); # Parsed message
    inWs = True;
    for c in tMessage.lower():
        if c in morseDict.keys():
            pMsg.append(c);
            inWs = False;
        else:
            if not inWs:
                pMsg.append(' ');
            #endif
            inWs = True;
        #endif
    #endfor

    if pMsg[-1] == ' ':
        pMsg.pop();
    #endif

    return pMsg;

#enddef

def output(msg):
  for c in msg:
    if c == WHITESPACE:
      print "~~~~~~~",;
    elif c == ILS:
      print "~~~",;
    else:
      for s in c:
        if s == '1':
          print ".",;
        elif s == '3':
          print "_",;
        else:
          sys.abort(1);
        #endif
      #endfor
    #endif
  #endfor





def main():

    rMsg = raw_input("Message: ");

    pMsg = parse(rMsg);

    eMsg = encodeMsg(pMsg);

    output(eMsg);

#enddef


if __name__ == "__main__":
    main();
